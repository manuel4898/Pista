﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : Movement
{
    public override void Start()
    {
        base.Start();
        speed = 500;
    }
    public override void Update()
    {
        hAcc = Input.acceleration.x;
        hSpd += hAcc;
        if (hSpd > maxSpeed / 4f) hSpd = maxSpeed / 4f;
        if (hSpd < -maxSpeed / 4f) hSpd = -maxSpeed / 4f;
        position.x -= hSpd;
        base.Update();
    }
}
