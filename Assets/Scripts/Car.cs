﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : Movement
{
    public override void Start()
    {
        base.Start();
        speed = 400;
    }
    public override void Update()
    {
        if (position.z < 5) position.x = Random.Range(-60, 75);
        base.Update();
    }
}
