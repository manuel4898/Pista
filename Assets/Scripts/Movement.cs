﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Movement : MonoBehaviour
{
    public float hAcc;
    public float hSpd;
    public float acc;
    public float maxAcc = 1f;
    public float speed;
    public float maxSpeed = 5f;

    protected Vector3 position;
    virtual public void Start()
    {
        position = this.transform.position;
    }
    virtual public void Update()
    {

        /*
        if (Input.touchCount > 0 || Input.GetKey(KeyCode.Space))
        {
            acc += Time.deltaTime / 10f;
            if (acc > maxAcc) acc = maxAcc;

            speed += acc;
            if (speed > maxSpeed) speed = maxSpeed;
        }
        else
        {
            acc = 0;
            speed -= Time.deltaTime * 2.5f;
            if (speed < 0) speed = 0;
        }*/
        
        position.z -= speed * Time.deltaTime;
        if (position.z < 0) position.z = 2000; 
        this.transform.position = position;
    }
}
